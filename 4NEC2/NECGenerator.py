import textwrap


class NECGenerator():
	
	"""
	This class consists of specific functions that are
	intrinsic to the commands of 4NEC2. A quick reference of the various
	commands may be found in the "Nec2 Short reference card" document.
	Further details on each of the commands are given in the "Nec2"
	document.
	"""
	
	def __init__(self, ):
		self.cnt = 1
		self.NECFile = ""
	
	
	def CM(self, text, segLen = 80):
		self.NECFile += "%sCE\n" % lineFormat("CM ", text, segLen)
	
	
	def GW(self, segNum, startCoor, endCoor, wireRad):	
		"""
		Input coordinates are tuples.
		"""
		
		startCoor += " %s" % endCoor		# Concatenate startCoor and endCoor to simplify processing
		coord = str(startCoor).strip('(),')
		
		finalText = "GW %s %s %s %s" % (str(self.cnt), str(segNum), coord, wireRad)
		self.cnt += 1
		
		self.NECFile += "%s\n" % finalText
	

	def GE(self, gndType):
		self.NECFile += "GE %s\n" % str(gndType)
	
	
	def EK(self):
		self.NECFile += "EK \n"
	
	
	# Loading functions
	
	def LD(self, ldType, ldTag = '', ldTagF = '', ldTagT = '', zlr = '', zli = '', zlc = ''):
		if(ldType == '-1'):
			self.NECFile += "LD -1\n"
		else:
			self.NECFile += "LD %s %s %s %s %s %s %s\n" % (ldType, ldTag, ldTagF, ldTagT, zlr, zli, zlc)
	
	
	# Excitation Functions
	
	def EX(self, i1 = '', i2 = '', i3 = '', i4 = '', f1 = '', f2 = ''):
		self.NECFile += "EX %s %s %s %s %s %s\n" % (i1, i2, i3, i4, f1, f2)
	
	
	def GN(self, type, radNum = '', dielectricConst= '', conductivity = '', radScreen = '', radWires = ''):
		type = str.lower(type)
		
		if(type == "free space" or type == '-1' or type == -1):
			self.NECFile += "GN -1\n"
		
		elif(type == "finite ground" or type == '0' or type == 0):
			self.NECFile += "GN %s %s 0 0 %s %s %s %s\n" % (type, radNum, dielectricConst, conductivity, radScreen, radWires)
		
		elif(type == "perfect ground" or type == '1' or type == 1):
			self.NECFile += "GN 1\n"
		
		elif(type == "sommer norton" or type == '2' or type == 2):
			self.NECFile += "GN %s 0 0 0 %s %s\n" % (type, dielectricConst, conductivity)
	
	
	def FR(self, stepNum, startMc, stepSize):
		self.NECFile += "FR 0 %s 0 0 %s %s\n" % (stepNum, startMc, stepSize)
	
	
	def EN(self):
		self.NECFile += "EN\n"
	
	
	def Write(self, inputFile):
		"""
		Write the complete contents of the NECFile.
		"""
		
		inputFile.write(self.NECFile)
	
	
	def Restart(self):
		self.cnt = 1
		self.NECFile = ""




# Separate lineFormat function

def lineFormat(prefix, cm, segLen = 80, wrap = True):
		if(wrap):
			text = textwrap.wrap(cm, segLen - len(prefix))
			finalText = ""
			for i in range(len(text)):
				finalText += "%s%s\n" % (prefix, text[i])
		else:
			return "%s%s\n" % (prefix, cm)
		
		return finalText
