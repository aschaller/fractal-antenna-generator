import math


class UndrawnTurtle():
	
    def __init__(self):
        self.x, self.y, self.angle = 0.0, 0.0, 0.0
        self.pointsVisited = []
        self._visit()

    def position(self):
		
        return self.x, self.y

    def xcor(self):
		
        return self.x

    def ycor(self):
		
        return self.y

    def forward(self, distance):
		
        angle_radians = math.radians(self.angle)

        self.x = round(self.x + math.cos(angle_radians) * distance, 3)
        self.y = round(self.y + math.sin(angle_radians) * distance, 3)

        self._visit()

    def backward(self, distance):
		
        self.forward(-distance)

    def right(self, angle):
		
        self.angle -= angle

    def left(self, angle):
		
        self.angle += angle

    def setpos(self, x, y = None):
		
        """Can be passed either a tuple or two numbers."""
        if y == None:
            self.x = x[0]
            self.y = y[1]
        else:
            self.x = x
            self.y = y
        self._visit()

    def _visit(self):
		
        """Add point to the list of points gone to by the turtle."""
        self.pointsVisited.append(self.position())

    def restart(self):

        self.pointsVisited = self.pointsVisited[:1]
	
	
    # Now for some aliases. Everything that's implemented in this class
    # should be aliased the same way as the actual api.
    fd = forward
    bk = backward
    back = backward
    rt = right
    lt = left
    setposition = setpos
    goto = setpos
    pos = position


def kochCurve(level, lengthSide):
	
	if level == 0:
		return
	
	lengthSide /= 3.0
	kochCurve(lengthSide, level - 1)
	ut.lt(60)
	kochCurve(lengthSide, level - 1)
	ut.rt(120)
	kochCurve(lengthSide, level - 1)
	ut.lt(60)
	kochCurve(lengthSide, level - 1)


def kochSnowflake(level, lengthSide):
	
	for i in range(3):
		kochCurve(lengthSide, level)
		ut.rt(120)


def kochCurveCntrPnt(level, segmentLen):
	
	return ((segmentLen * 3**level)/2, 0)


def kochSnowflakeCntrPnt(level, segmentLen):
	
	# Calculate one side-length of triangle
	s = segmentLen * 3**level
	
	# Calculate third vertex point
	v3 = -(math.sqrt(s**2 - (0.25 * (s**2))))
	
	# Calculate centroid; remember that the first coordinates is always (0, 0)
	x = (s + s/2.0)/3.0
	y = v3/3.0
	
	return (x, y)


def hilbert(level, angle):
	
	if level == 0:
		return
	
	ut.rt(angle)
	hilbert(level - 1, -angle)
	ut.fd(size)
	ut.lt(angle)
	hilbert(level - 1, angle)
	ut.fd(size)
	hilbert(level - 1, angle)
	ut.lt(angle)
	ut.fd(size)
	hilbert(level - 1, -angle)
	ut.rt(angle)

def hilbertSideLength(level, segmentLen):
    
    #Calculate one side-length of the square
    # Equation: f(x) = 2^n - 1
    
    totalLen = 2**level - 1
    totalLen *= segmentLen
    
    return totalLen

def hilbertCntrPnt(level, segmentLen):
	
	hilbertSideLength(level, segmentLen)
	
	x = -(totalLen/2)
	y = totalLen/2
	
	return (x, y)


ut = UndrawnTurtle()



##  Examples to construct Hilbert Curve, Koch Curve, and Koch Snowflake:
##
##  Hilbert Curve:
##
##
##  print "Enter size of segment for the Hilbert Curve."
##  size = input()
##
##  print "Enter iteration level for the Hilbert Curve."
##  hilbertLevel = input()
##
##
##  hilbertCenter = hilbertCntrPnt(hilbertLevel, size)
##  ut.setpos(hilbertCenter[0], hilbertCenter[1])
##  hilbert(hilbertLevel, 90)
##
### (0, 0) is still recorded, create new list of tuples without it
##  hilbertPoints = ut.pointsVisited[1:]
##
##
##  Koch Curve:
##
##
##  print "Enter size of segment for the Koch Curve."
##  kochSize = input()
##
##  print "Enter iteration level for the Koch Snowflake."
##  kochLevel = input()
##
##  kochCurveCntr = kochCurveCntrPnt(kochLevel, kochSize)
##  ut.setpos(kochCurveCntr[0], kochCurveCntr[1])
##
##  kochCurve(kochLevel, kochSize)
##
##
##
##  Koch Snowflake:
##
##
##  print "Enter the size of the segment for the Koch Snowflake."
##  kochSize = input()
##
##  print "Enter the iteration level for the Koch Snowflake."
##  kochLevel = input()
##
##  kochSnowflakeCntr = kochSnowflakeCntrPnt(kochLevel, kochSize)
##  ut.setpos(kochSnowflakeCntr[0], kochSnowflakeCntr[1])
##
##  kochSnowflake(kochLevel, kochSize)
